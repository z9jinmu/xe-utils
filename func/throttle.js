/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable prefer-rest-params */
/**
  * 节流函数；当被调用 n 毫秒后才会执行，如果在这时间内又被调用则至少每隔 n 秒毫秒调用一次该函数
  *
  * @param {Function} callback 回调
  * @param {Number} wait 多少秒毫
  * @param {Object} options 参数{leading: 是否在之前执行, trailing: 是否在之后执行}
  * @return {Function}
  */
function throttle (callback, wait, options) {
  const opts = options || {}
  let runFlag = false
  let timeout = 0
  const optLeading = 'leading' in opts ? opts.leading : true
  const optTrailing = 'trailing' in opts ? opts.trailing : false
  const runFn = function (args, context) {
    runFlag = true
    callback.apply(context, args)
    timeout = setTimeout(endFn, wait)
  }
  var endFn = function (args, context) {
    timeout = 0
    if (!runFlag && optTrailing === true) {
      runFn(args, context)
    }
  }
  const cancelFn = function () {
    const rest = timeout !== 0
    clearTimeout(timeout)
    runFlag = false
    timeout = 0
    return rest
  }
  const throttled = function () {
    const args = arguments
    const context = this
    runFlag = false
    if (timeout === 0) {
      if (optLeading === true) {
        runFn(args, context)
      } else if (optTrailing === true) {
        timeout = setTimeout(() => endFn(args, context), wait)
      }
    }
  }
  throttled.cancel = cancelFn
  return throttled
}

module.exports = throttle
